import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:youtube_native_player/android/constrant.dart';
import 'package:youtube_native_player/android/video_player_controller.dart';
import 'package:youtube_native_player/youtube_native_player.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: MyHomePage(title: 'Youtube Player Demo'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  static const platform = const MethodChannel("com.lolq.youtubenativeplayer");
  VideoPlayerController _videoController;
  String position = "Get Current Position";
  String status = "Get Player Status";
  String videoDuration = "Get Video Duration";
//  String _source = "ZLYcd3h63ow";
  String _source = "JiQM5kFP41E";
  bool isMute = false;

  @override
  void initState() {
    getSharedVideoUrl();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            YoutubeNativePlayer(
              context: context,
              source: _source,
              quality: YoutubeQuality.HD,
              aspectRatio: 16 / 9,
              autoPlay: true,
              loop: false,
              reactToOrientationChange: true,
              startFullScreen: false,
              controlsActiveBackgroundOverlay: true,
              controlsTimeOut: Duration(seconds: 4),
              playerMode: YoutubePlayerMode.DEFAULT,
              switchFullScreenOnLongPress: true,
              callbackController: (controller) {
                _videoController = controller;
              },
              onError: (error) {
                print(error);
              },
              onVideoEnded: () => _showThankYouDialog(),
              onVideoStarted: (duration) => _startedVideo(duration),
            ),
            SizedBox(
              height: 10.0,
            ),
          ],
        ),
      ),
    );
  }

  void _showThankYouDialog() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("Video Ended"),
          content: Text("Thank you for trying the plugin!"),
        );
      },
    );
  }


  void _startedVideo(int duration) {
    String result = (null != _videoController) ? "DURATION : " + duration.toString() : "DURATION : ";
    print("_startedVideoDialog > result :: " + result);
  }


  getSharedVideoUrl() async {
    if (defaultTargetPlatform == TargetPlatform.android) {
      try {
        var sharedData = await platform.invokeMethod("getSharedYoutubeVideoUrl");
        if (sharedData != null && mounted) {
          setState(() {
            _source = sharedData;
            print(_source);
          });
        }
      } on PlatformException catch (e) {
        print(e.message);
      }
    }
  }
}
