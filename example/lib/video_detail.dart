import 'package:flutter/material.dart';
import 'package:youtube_native_player/android/constrant.dart';
import 'package:youtube_native_player/youtube_native_player.dart';

class VideoDetail extends StatefulWidget {
  @override
  _VideoDetailState createState() => _VideoDetailState();
}

class _VideoDetailState extends State<VideoDetail> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Video Detail"),
      ),
      body: YoutubeNativePlayer(
        context: context,
        source: "mH2bq1CVNoU",
        quality: YoutubeQuality.HD,
        aspectRatio: 16 / 9,
      ),
    );
  }
}
