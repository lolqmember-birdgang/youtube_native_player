#import "YoutubeNativePlayerPlugin.h"
#import <youtube_native_player/youtube_native_player-Swift.h>

@implementation YoutubeNativePlayerPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftYoutubeNativePlayerPlugin registerWithRegistrar:registrar];
}
@end
