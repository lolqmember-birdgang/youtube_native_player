import Foundation
import Flutter

class YoutubeNativePlayerFactory: NSObject, FlutterPlatformViewFactory {
    let registrar: FlutterPluginRegistrar
    init(_registrar: FlutterPluginRegistrar) {
        registrar = _registrar
        super.init()
    }

    func createArgsCodec() -> FlutterMessageCodec & NSObjectProtocol {
        return FlutterStandardMessageCodec.sharedInstance()
    }

    func create(withFrame frame: CGRect, viewIdentifier viewId: Int64, arguments args: Any?) -> FlutterPlatformView {
        return YoutubeNativePlayerView(_frame: frame,
                                  _viewId: viewId,
                                  _params: args as? Dictionary<String, Any> ?? nil,
                                  _registrar: registrar)
    }
}
