import Flutter
import UIKit

public class SwiftYoutubeNativePlayerPlugin: NSObject, FlutterPlugin {
    public static func register(with registrar: FlutterPluginRegistrar) {
        registrar.register(
            YoutubeNativePlayerFactory(_registrar: registrar),
            withId: YPConstant.METHOD_CALL_NAME
        )
    }
}
