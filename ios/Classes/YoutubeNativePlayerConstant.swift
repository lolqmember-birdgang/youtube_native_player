//
//  YoutubeNativePlayerConstant.swift
//  app_review
//
//  Created by newhowl on 17/06/2019.
//

import Foundation

struct YPConstant{
    static let METHOD_CALL_NAME = "com.lolq.youtubenativeplayer";
    static let YOUTUBE_URL_HEADER = "https://www.youtube.com/watch?v=";
}

struct YPParamConstant{
    static let YOUTUBE_ID = "youtubeId";
    static let WIDTH = "width";
    static let HEIGHT = "height";
}

struct YPHandleConstant{
    static let INITIALIZATION = "initialization";
    static let PLAY = "play";
    static let PAUSE = "pause";
    static let SEEK_TO = "seekTo";
    static let CHANGE_SCALE = "changeScale";
    static let PART_SCREEN = "partScreen";
    static let FULL_SCREEN = "fullScreen";
}

struct YPInvokeMethod{
    static let ON_CURRENT_SECOND = "onCurrentSecond";
    static let ON_STARTED = "onStarted";
    static let ON_VIDEO_DURATION = "onVideoDuration";
    static let ON_STATE_CHANGE = "onStateChange";
}

struct YPInvokeMethodArg{
    static let PLAY = "play";
    static let PAUSE = "pause";
}
