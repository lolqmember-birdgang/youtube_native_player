import UIKit
import AVFoundation
import AVKit

class YoutubeNativePlayerController: AVPlayerViewController {
    var isFullScreen: Bool = false
    var channel: FlutterMethodChannel? = nil
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return isFullScreen ? .landscape : .portrait
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        print("YoutubeNativePlayerController > viewDidDisappear")
        channel?.invokeMethod("onFullScreen", arguments: false)
    }
}
