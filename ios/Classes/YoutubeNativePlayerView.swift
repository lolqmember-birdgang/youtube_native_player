
import Foundation
import Flutter
import AVKit
import AVFoundation
import YoutubeDirectLinkExtractor


class YoutubeNativePlayerView: NSObject, FlutterPlatformView {
    private let frame: CGRect
    private let viewId: Int64
    private let registrar: FlutterPluginRegistrar
    private let params: [String: Any]
    private let playerView: UIView
    private let channel: FlutterMethodChannel
    private var player: AVPlayer!
    private var playerObserverToken: Any!
    private var playerItem: AVPlayerItem!
    private var playerItemContext: Int = 0
    private var playerLayer: YoutubeNativePlayerLayer!
    private var playerController: YoutubeNativePlayerController!
    private var playerDelegate: AVPlayerViewControllerDelegate!
    private var isPlayerReady = false

    private var duration : CMTime!
    private var seconds : Float64!

    /**
    * FlutterPlaytformView
    **/
    //
    init(_frame: CGRect,
         _viewId: Int64,
         _params: [String: Any]?,
         _registrar: FlutterPluginRegistrar) {
        frame = _frame
        viewId = _viewId
        registrar = _registrar
        params = _params!
        playerView = UIView(frame: frame)
        playerView.backgroundColor = UIColor.black
        channel = FlutterMethodChannel(
            name: "\(YPConstant.METHOD_CALL_NAME)/\(viewId)",
            binaryMessenger: registrar.messenger()
        )
        super.init()
        self.initPlayer()
        channel.setMethodCallHandler { [weak self] (call, result) in
            guard let `self` = self else { return }
            `self`.handle(call, result: result)
        }
    }
    
    //
    deinit {
        dispose()
        deinitObjerver()
        print("YoutubeNativePlayerView is deninit")
    }

    
    //
    func view() -> UIView {
        return playerView
    }

    //
    private func dispose() {
        playerPause()
    }

    
    
    /**
    * init
    **/
    private func initPlayer() {
        print("params = \(params)")
        let youtubeId = params[YPParamConstant.YOUTUBE_ID] as? String

        let y = YoutubeDirectLinkExtractor()
        let youtubeUrl = YPConstant.YOUTUBE_URL_HEADER  + youtubeId!
        y.extractInfo(for: .urlString(youtubeUrl),
                      success: {info in
                        DispatchQueue.main.async {
                            self.player = AVPlayer(url: URL(string: info.highestQualityPlayableLink!)!)

                            self.initPlayerItem()
                            self.initPlayerLayer()
                            self.initPlayerController()
                            self.initPlayerView()
                            self.initSlider()
                            self.initSliderObserver()
                            self.initComplete()
                        }
        }) { error in
            print(error)
        }
    }
    
    
    func initPlayerItem() {
        self.playerItem = self.player.currentItem
        self.playerItem.addObserver(self, forKeyPath: #keyPath(AVPlayerItem.status), options: [.old, .new], context: &playerItemContext)
    }
    
    func initPlayerLayer() {
        self.playerLayer = YoutubeNativePlayerLayer()
        self.playerLayer.player = self.player
        self.playerLayer.frame = self.playerView.frame
        self.playerLayer.videoGravity = .resizeAspect
        self.playerLayer.needsDisplayOnBoundsChange = true
    }
    
    func initPlayerController() {
        self.playerController = YoutubeNativePlayerController()
        self.playerController.player = self.player
        self.playerController.view = self.playerView
        self.playerController.channel = self.channel
        if #available(iOS 9.0, *) {
            self.playerController.delegate = self
        } else {
            // Fallback on earlier versions
        }
    }
    
    func initPlayerView() {
        self.playerView.layer.addSublayer(self.playerLayer)
        self.playerView.layoutIfNeeded()
    }

    func initSlider() {
        self.duration = self.playerItem.asset.duration
        self.seconds = CMTimeGetSeconds(self.duration)
    }
    
    func initSliderObserver() {
        let interval = CMTimeMakeWithSeconds(1, 1)
        self.playerObserverToken = self.player!.addPeriodicTimeObserver(
            forInterval: interval,
            queue: DispatchQueue.main)
        { (CMTime) -> Void in
            if self.player!.currentItem?.status == .readyToPlay {
                let time : Float64 = CMTimeGetSeconds(self.player!.currentTime());
                
                self.channel.invokeMethod(YPInvokeMethod.ON_CURRENT_SECOND, arguments: time)
            }
        }
    }
    
    func initComplete() {
        let durationInt: Int = Int(self.seconds)
        let durationDouble: Double = Double(self.seconds)
        
        self.channel.invokeMethod(YPInvokeMethod.ON_STARTED, arguments: durationInt)
        self.channel.invokeMethod(YPInvokeMethod.ON_VIDEO_DURATION, arguments: durationDouble)
    }
    
    func deinitObjerver() {
        if ((self.playerObserverToken) != nil) {
            self.player!.removeTimeObserver(self.playerObserverToken)
            self.playerObserverToken = nil
        }
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        guard context == &self.playerItemContext else {
            super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
            return
        }
        
        if keyPath == #keyPath(AVPlayerItem.status) {
            let status: AVPlayerItemStatus
            if let statusNumber = change?[.newKey] as? NSNumber {
                status = AVPlayerItemStatus(rawValue: statusNumber.intValue)!
            } else {
                status = .unknown
            }
            
            switch status {
            case .readyToPlay:
                print("observeValue readyToPlay")
                self.isPlayerReady = true;
                
                playerPlay()
                break
            case .failed:
                print("observeValue failed")
                break
            case .unknown:
                print("observeValue unknown")
                break
            }
        }
    }
    
    
    /**
     * handle
     **/
    func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
        switch call.method {
        case YPHandleConstant.INITIALIZATION:
            result(nil)
        case YPHandleConstant.PLAY:
            play(call)
            result(nil)
        case YPHandleConstant.PAUSE:
            pause(call)
            result(nil)
        case YPHandleConstant.SEEK_TO:
            seekTo(call)
            result(nil)
        case YPHandleConstant.CHANGE_SCALE:
            changeScale(call)
            result(nil)
        case YPHandleConstant.PART_SCREEN:
            partScreen(call)
            result(nil)
        case YPHandleConstant.FULL_SCREEN:
            fullScreen(call)
            result(nil)
        default:
            result(FlutterMethodNotImplemented)
        }
    }
    
    
    /**
     * handle function
     **/
    // play
    func play(_ call: FlutterMethodCall) {
        print("play is called")
        if (self.isPlayerReady) {
            playerPlay()
        }
    }
    
    
    // pause
    func pause(_ call: FlutterMethodCall) {
        print("pause is called")
        if (self.isPlayerReady) {
            playerPause()
        }
    }
    
    
    // seekTo
    func seekTo(_ call: FlutterMethodCall) {
        print("seekTo is called")
        if (self.isPlayerReady) {
            let seconds = call.arguments as! Double
            let targetTime:CMTime = CMTimeMake(Int64(seconds), 1)
            
            self.player!.seek(to: targetTime)
            
            if self.player!.rate == 0 {
                playerPlay()
            }
        }
    }
    
    
    // change scale
    func changeScale(_ call: FlutterMethodCall) {
        print("scale is called")
        if (self.isPlayerReady) {
            let scaleParam = call.arguments as! Dictionary<String, Any>
            let paramWidth = scaleParam[YPParamConstant.WIDTH] as? Double
            let paramHeight = scaleParam[YPParamConstant.HEIGHT] as? Double
            
            self.playerView.frame = CGRect(x: 0.0, y: 0.0, width: paramWidth!, height: paramHeight!)
            self.playerLayer.frame = self.playerView.frame;
            self.playerView.layoutIfNeeded()
        }
    }
    
    
    //part screen (landscape)
    func partScreen(_ call: FlutterMethodCall) {
        print("viewScreen is called")
        if (self.isPlayerReady) {
            self.playerController.isFullScreen = false
            
            playerPlay()
            
            UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")
        }
    }
    
    
    
    // full screen (portrait)
    func fullScreen(_ call: FlutterMethodCall) {
        print("fullScreen is called")
        if (self.isPlayerReady) {
            self.playerController.isFullScreen = true
            
            let keyWindow = UIApplication.shared.keyWindow
            let viewController = keyWindow?.rootViewController
            viewController?.present(self.playerController, animated: true) {
                self.playerPlay()
            }
            
            UIDevice.current.setValue(UIInterfaceOrientation.landscapeRight.rawValue, forKey: "orientation")
        }
    }
    
    
    /**
    *
    **/
    func playerPlay() {
        if self.playerController.isFullScreen {
            self.playerController.player?.play()
        } else {
            self.player?.play()
        }
        self.channel.invokeMethod(YPInvokeMethod.ON_STATE_CHANGE, arguments: YPInvokeMethodArg.PLAY)
    }
    
    func playerPause() {
        if self.playerController.isFullScreen {
            self.playerController.player?.pause()
        } else {
            self.player?.pause()
        }
        self.channel.invokeMethod(YPInvokeMethod.ON_STATE_CHANGE, arguments: YPInvokeMethodArg.PAUSE)
    }
}


extension YoutubeNativePlayerView: AVPlayerViewControllerDelegate {
    func playerViewController(_ playerViewController: YoutubeNativePlayerController, restoreUserInterfaceForPictureInPictureStopWithCompletionHandler completionHandler: (Bool) -> Void) {
//        playerViewController?.presentViewController(playerViewController, animated: true) {
//            completionHandler(true)
//        }

    }
}
