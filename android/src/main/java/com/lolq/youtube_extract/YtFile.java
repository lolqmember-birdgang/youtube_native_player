package com.lolq.youtube_extract;

public class YtFile {

    private Format format;
    private String url = "";

    public YtFile(Format format, String url) {
        this.format = format;
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public Format getFormat() {
        return format;
    }

    @Deprecated
    public Format getMeta() {
        return format;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        YtFile ytFile = (YtFile) o;

        if (format != null ? !format.equals(ytFile.format) : ytFile.format != null) {
            return false;
        }

        return url != null ? url.equals(ytFile.url) : ytFile.url == null;
    }

    @Override
    public int hashCode() {
        int result = format != null ? format.hashCode() : 0;
        result = 31 * result + (url != null ? url.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "YtFile {" +
                "format=" + format +
                ", url='" + url + '\'' +
                '}';
    }
}
