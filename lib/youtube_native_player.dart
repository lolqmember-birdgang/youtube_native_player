import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:youtube_native_player/android/constrant.dart';
import 'package:youtube_native_player/android/controls_color.dart';
import 'package:youtube_native_player/android/youtube_native_player_android.dart';
import 'package:youtube_native_player/constants/youtube_native_player_constant.dart';
import 'package:youtube_native_player/ios/youtube_native_player_ios.dart';

import 'ios/video_orientations.dart';


class YoutubeNativePlayer extends StatefulWidget {
  final String source;
  final YoutubeQuality quality;
  final BuildContext context;

  final double aspectRatio;
  final double width;

  final bool autoPlay;
  final bool isLive;

  final ControlsColor controlsColor;

  final bool controlsActiveBackgroundOverlay;

  final Duration controlsTimeOut;
  final Duration startAt;

  final bool showThumbnail;
  final bool keepScreenOn;
  final bool showVideoProgressbar;
  final bool startFullScreen;

  final YPCallBack callbackController;

  final ErrorCallback onError;

  final NumberCallback onVideoStarted;
  final VoidCallback onVideoEnded;

  final YoutubePlayerMode playerMode;

  final bool switchFullScreenOnLongPress;
  final bool hideShareButton;
  final bool reactToOrientationChange;
  final bool loop;

  final VideoOrientations videoOrientations;

  YoutubeNativePlayer({
    @required this.source,
    @required this.context,
    @required this.quality,
    this.aspectRatio = 16 / 9,
    this.width,
    this.isLive = false,
    this.autoPlay = true,
    this.controlsColor,
    this.startAt,
    this.showThumbnail = false,
    this.keepScreenOn = true,
    this.showVideoProgressbar = true,
    this.startFullScreen = false,
    this.controlsActiveBackgroundOverlay = false,
    this.controlsTimeOut = const Duration(seconds: 3),
    this.playerMode = YoutubePlayerMode.DEFAULT,
    this.onError,
    this.onVideoStarted,
    this.onVideoEnded,
    this.callbackController,
    this.switchFullScreenOnLongPress = false,
    this.hideShareButton = false,
    this.reactToOrientationChange = true,
    this.loop = false,
    this.videoOrientations = const VideoOrientations(),

  }) : assert(
  (width ?? MediaQuery.of(context).size.width) <= MediaQuery.of(context).size.width, "Width must be less than Screen Width.\nScreen width:${MediaQuery.of(context).size.width}\nGiven width:$width");

  @override
  State<StatefulWidget> createState() {
    return _YoutubePlayerState();
  }
}

class _YoutubePlayerState extends State<YoutubeNativePlayer> with WidgetsBindingObserver {
  @override
  Widget build(BuildContext context) {
    if (defaultTargetPlatform == TargetPlatform.android) {
      return _androidViewWidget();
    } else if (defaultTargetPlatform == TargetPlatform.iOS) {
      return _iosViewWidget();
    }
    return Text('$defaultTargetPlatform is not yet supported by the text_view plugin');
  }

  Widget _androidViewWidget() {
    return YoutubeNativePlayerAndroid(
      source: widget.source,
      context: widget.context,
      quality: widget.quality,
      aspectRatio: widget.aspectRatio,
      width: widget.width,
      isLive: widget.isLive,
      autoPlay: widget.autoPlay,
      controlsColor: widget.controlsColor,
      startAt: widget.startAt,
      showThumbnail: widget.showThumbnail,
      keepScreenOn: widget.keepScreenOn,
      showVideoProgressbar: widget.showVideoProgressbar,
      startFullScreen: widget.startFullScreen,
      controlsActiveBackgroundOverlay: widget.controlsActiveBackgroundOverlay,
      controlsTimeOut: widget.controlsTimeOut,
      playerMode: widget.playerMode,
      onError: widget.onError,
      onVideoStarted: widget.onVideoStarted,
      onVideoEnded: widget.onVideoEnded,
      callbackController: widget.callbackController,
      switchFullScreenOnLongPress: widget.switchFullScreenOnLongPress,
      hideShareButton: widget.hideShareButton,
      reactToOrientationChange: widget.reactToOrientationChange,
      loop: widget.loop,
    );
  }

  Widget _iosViewWidget() {
    return YoutubeNativePlayerIOS(
      source: widget.source,
      aspectRatio: widget.aspectRatio,
      width: widget.width,
      onError: widget.onError,
      onVideoStarted: widget.onVideoStarted,
      onVideoEnded: widget.onVideoEnded,
      videoOrientations: widget.videoOrientations,
    );
  }
}












