enum YoutubeQuality {
  /// = 144p
  LOWEST,
  /// = 240p
  LOW,
  /// = 360p
  MEDIUM,
  /// = 480p
  HIGH,
  /// = 720p
  HD,
  /// = 1080p
  FHD,
}

enum YoutubePlayerMode {
  DEFAULT,
  NO_CONTROLS,
}