// Copyright 2019 Sarbagya Dhaubanjar. All rights reserved.
// Use of this source code is governed by a MIT license that can be found
// in the LICENSE file.

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:youtube_native_player/android/constrant.dart';
import 'package:youtube_native_player/android/controls_color.dart';
import 'package:youtube_native_player/android/video_player.dart';
import 'package:youtube_native_player/android/video_player_controller.dart';
import 'package:youtube_native_player/android/video_progress_colors.dart';
import 'package:youtube_native_player/android/video_progress_indicator.dart';
import 'package:youtube_native_player/constants/youtube_native_player_constant.dart';
import 'package:youtube_native_player/controls.dart';

final MethodChannel _channel = const MethodChannel('com.lolq/youtube_native_player')..invokeMethod('init');

enum DataSourceType { asset, network, file }


Future<Map<dynamic, dynamic>> requestCreate(Map<dynamic, dynamic> dataSourceDescription,) async {
  return await _channel.invokeMethod(
    'create', dataSourceDescription,
  );
}

Future<void> requestDispose (int textureId,) async {
  return await _channel.invokeMethod(
    'dispose', <String, dynamic>{'textureId': textureId},
  );
}

Future<void> requestSetLooping (int textureId, bool isLooping,) async {
  return await _channel.invokeMethod(
    'setLooping',
    <String, dynamic>{'textureId': textureId, 'looping': isLooping},
  );
}

Future<void> requestPlay (int textureId,) async {
  return await _channel.invokeMethod(
    'play',
    <String, dynamic>{'textureId': textureId,},
  );
}

Future<void> requestPause (int textureId,) async {
  return await _channel.invokeMethod(
    'pause', <String, dynamic>{'textureId': textureId},
  );
}

Future<void> requestSetVolume (int textureId, double volume,) async {
  return await _channel.invokeMethod(
    'setVolume', <String, dynamic>{'textureId': textureId, 'volume': volume},
  );
}

Future<int> requestPosition (int textureId,) async {
  return await _channel.invokeMethod(
    'position', <String, dynamic>{'textureId': textureId},
  );
}

Future<int> requestDuration (int textureId,) async {
  return await _channel.invokeMethod(
    'duration', <String, dynamic>{'textureId': textureId},
  );
}


Future<int> requestSeekTo (int textureId, int milliseconds) async {
  return await _channel.invokeMethod('seekTo', <String, dynamic>{
    'textureId': textureId,
    'location': milliseconds,
  });
}



class YoutubeNativePlayerAndroid extends StatefulWidget {
  final String source;
  final YoutubeQuality quality;
  final BuildContext context;

  final double aspectRatio;
  final double width;

  final bool autoPlay;
  final bool isLive;

  final ControlsColor controlsColor;

  final bool controlsActiveBackgroundOverlay;

  final Duration controlsTimeOut;
  final Duration startAt;

  final bool showThumbnail;
  final bool keepScreenOn;
  final bool showVideoProgressbar;
  final bool startFullScreen;

  final YPCallBack callbackController;
  final ErrorCallback onError;
  final NumberCallback onVideoStarted;
  final VoidCallback onVideoEnded;
  final YoutubePlayerMode playerMode;

  final bool switchFullScreenOnLongPress;
  final bool hideShareButton;
  final bool reactToOrientationChange;
  final bool loop;

  YoutubeNativePlayerAndroid({
    @required this.source,
    @required this.context,
    @required this.quality,
    this.aspectRatio = 16 / 9,
    this.width,
    this.isLive = false,
    this.autoPlay = true,
    this.controlsColor,
    this.startAt,
    this.showThumbnail = false,
    this.keepScreenOn = true,
    this.showVideoProgressbar = true,
    this.startFullScreen = false,
    this.controlsActiveBackgroundOverlay = false,
    this.controlsTimeOut = const Duration(seconds: 3),
    this.playerMode = YoutubePlayerMode.DEFAULT,
    this.onError,
    this.onVideoStarted,
    this.onVideoEnded,
    this.callbackController,
    this.switchFullScreenOnLongPress = false,
    this.hideShareButton = false,
    this.reactToOrientationChange = true,
    this.loop = false,
  }) : assert(
  (width ?? MediaQuery.of(context).size.width) <= MediaQuery.of(context).size.width, "Width must be less than Screen Width.\nScreen width:${MediaQuery.of(context).size.width}\nGiven width:$width");

  @override
  State<StatefulWidget> createState() {
    return _YoutubePlayerState();
  }

  static Future<double> get brightness async => (await _channel.invokeMethod('brightness')) as double;
  static Future setBrightness(double brightness) => _channel.invokeMethod('setBrightness', {"brightness": brightness});
  static Future<bool> get isKeptOn async => (await _channel.invokeMethod('isKeptOn')) as bool;
  static Future keepOn(bool on) => _channel.invokeMethod('keepOn', {"on": on});
}


class _YoutubePlayerState extends State<YoutubeNativePlayerAndroid> with WidgetsBindingObserver {
  VideoPlayerController _videoController;
  String videoId = "";
  bool initialize = true;
  double width;
  double height;
  bool _showControls;
  String _selectedQuality;
  bool _showVideoProgressBar = true;
  ControlsColor controlsColor;

  bool _isFullScreen = false;
  bool _triggeredByUser = false;
  bool _videoEndedCalled = false;
  bool _videoStaredCalled = false;

  @override
  void initState() {
    super.initState();
    _selectedQuality = qualityMapping(widget.quality);
    _showControls = widget.autoPlay ? false : true;
    if (widget.source.contains("http")) {
      videoId = getIdFromUrl(widget.source);
    } else {
      videoId = widget.source;
    }

    if (videoId != null) {
      String dataSource = "${videoId}birdgang${_selectedQuality}birdgang${widget.isLive}";
      print("initState > dataSource :: " + dataSource);
      _videoController = VideoPlayerController.network(dataSource);
    }

    if (controlsColor == null) {
      controlsColor = ControlsColor();
    } else {
      if (widget.controlsActiveBackgroundOverlay)
        controlsColor = ControlsColor(
          buttonColor: widget.controlsColor.buttonColor,
          controlsBackgroundColor: Colors.transparent,
          playPauseColor: widget.controlsColor.playPauseColor,
          progressBarPlayedColor: widget.controlsColor.progressBarPlayedColor,
          progressBarBackgroundColor:
          widget.controlsColor.progressBarBackgroundColor,
          seekBarPlayedColor: widget.controlsColor.seekBarPlayedColor,
          seekBarUnPlayedColor: widget.controlsColor.seekBarUnPlayedColor,
          timerColor: widget.controlsColor.timerColor,
        );
    }
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    _videoController?.dispose();
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeMetrics() {
    if (widget.reactToOrientationChange) {
      double w = WidgetsBinding.instance.window.physicalSize.width;
      double h = WidgetsBinding.instance.window.physicalSize.height;
      // Switched to LandScape Mode
      if (w > h && !_isFullScreen) {
        _pushFullScreenWidget(context, false);
      }
      // Switched to Portrait Mode
      if (w < h && _isFullScreen && !_triggeredByUser) {
        Navigator.pop(context);
      }
    }
    super.didChangeMetrics();
  }

  void initializeYTController() {
    _videoController.initialize().then((_) {
        if (widget.autoPlay) {
          _videoController.play();
        }

        if (mounted) {
          setState(() {});
        }
        if (widget.startFullScreen) {
          _pushFullScreenWidget(context);
        }
        if (widget.startAt != null) _videoController.seekTo(widget.startAt);
        _videoController.addListener(listener);
      },
    );
    if (widget.callbackController != null) {
      widget.callbackController(_videoController);
    }
    print("Youtube Video Id: $videoId");
  }

  listener() {
    if (_videoController.value.position == _videoController.value.duration && !_videoEndedCalled) {
      widget.onVideoEnded();
      if (widget.loop) {
        _videoController.seekTo(
          Duration(seconds: 0),
        );
      }
      _videoEndedCalled = true;
    } if (_videoController.value.position.inSeconds > 0 && !_videoStaredCalled) {
      widget.onVideoStarted(_videoController.value.duration.inSeconds);
      _videoEndedCalled = false;
      _videoStaredCalled = true;
    }
    else {
      _videoEndedCalled = false;
    }
  }

  @override
  Widget build(BuildContext context) {
    if (widget.keepScreenOn) {
      YoutubeNativePlayerAndroid.keepOn(true);
    }

    width = widget.width ?? MediaQuery.of(context).size.width;
    height = 1 / widget.aspectRatio * width;
    if (widget.source.contains("http")) {
      if (getIdFromUrl(widget.source) != videoId) {
        _videoController.pause();
        videoId = getIdFromUrl(widget.source);
        if (videoId != null) {
          _videoController = VideoPlayerController.network("${videoId}birdgang${_selectedQuality}birdgang${widget.isLive}");
          initializeYTController();
        } else {
          widget.onError("Malformed Video ID or URL");
        }
      }
    } else {
      if (widget.source != videoId) {
        _videoController.pause();
        videoId = widget.source;
        if (videoId != null) {
          _videoController = VideoPlayerController.network("${videoId}birdgang${_selectedQuality}birdgang${widget.isLive}");
          initializeYTController();
        }
      }
    }
    if (initialize && videoId != null) {
      initializeYTController();
      initialize = false;
    }
    return _buildVideo(height, width, false);
  }


  Widget _buildVideo(double _height, double _width, bool _isFullScreen) {
    return AnimatedContainer(
      duration: Duration(seconds: 1),
      height: _height,
      width: _width,
      color: Colors.black,
      child: AspectRatio(
        aspectRatio: _videoController.value.aspectRatio,
        child: Stack(
          alignment: Alignment(0, 0),
          children: <Widget>[

            AnimatedContainer(
              duration: Duration(seconds: 1),
              height: _height,
              width: _width,
              decoration: widget.showThumbnail && videoId != null ?
                BoxDecoration(
                  image: DecorationImage(
                    image: NetworkImage("https://i3.ytimg.com/vi/$videoId/sddefault.jpg"),
                    fit: BoxFit.cover,
                  ),
                )
                : BoxDecoration(),
            ),

            Center(
              child: _videoController.value.initialized ?
                AnimatedContainer(
                  duration: Duration(seconds: 1),
                  height: _height,
                  child: AspectRatio(
                    aspectRatio: _videoController.value.aspectRatio,
                    child: VideoPlayer(_videoController),
                  ),
                )
                : Container(
                  height: _height,
                  width: _width,
                  child: Center(
                    child: CircularProgressIndicator(),
                  ),
                ),
            ),


            AnimatedContainer(
              duration: Duration(seconds: 1),
              height: _height,
              width: _width,
              child: _videoController.value.initialized && widget.playerMode == YoutubePlayerMode.DEFAULT ?
                Controls(
                  height: _height,
                  width: _width,
                  isLive: widget.isLive,
                  controller: _videoController,
                  showControls: _showControls,
                  videoId: videoId,
                  defaultQuality: _selectedQuality,
                  isFullScreen: _isFullScreen,
                  controlsActiveBackgroundOverlay:
                  widget.controlsActiveBackgroundOverlay,
                  controlsColor: controlsColor,
                  controlsTimeOut: widget.controlsTimeOut,
                  switchFullScreenOnLongPress:
                  widget.switchFullScreenOnLongPress,
                  controlsShowingCallback: (showing) {
                    Timer(Duration(milliseconds: 600), () {
                      if (mounted)
                        setState(() {
                          _showVideoProgressBar = !showing;
                        });
                    });
                  },
                  qualityChangeCallback: (quality, position) {
                    _videoController.pause();
                    if (mounted) {
                      setState(() {
                        _selectedQuality = quality;
                        if (videoId != null)
                          _videoController = VideoPlayerController.network("${videoId}birdgang${_selectedQuality}birdgang${widget.isLive}");
                      });
                    }
                    _videoController.initialize().then((_) {
                      _videoController.seekTo(position);
                      _videoController.play();
                      if (mounted) {
                        setState(() {});
                      }
                    });
                    if (widget.callbackController != null) {
                      widget.callbackController(_videoController);
                    }
                  },
                  fullScreenCallback: () async {
                    await _pushFullScreenWidget(context);
                  },
                  hideShareButton: widget.hideShareButton,
                )
                : Container(),
            ),


            _videoController.value.initialized && _showVideoProgressBar && widget.showVideoProgressbar && !_isFullScreen ?
            Positioned(
              bottom: -3.5,
              child: Container(
                width: _width,
                child: VideoProgressIndicator(
                  _videoController,
                  allowScrubbing: true,
                  colors: VideoProgressColors(
                    backgroundColor:
                    controlsColor.progressBarBackgroundColor,
                    playedColor: controlsColor.progressBarPlayedColor,
                  ),
                  padding: EdgeInsets.all(0.0),
                ),
              ),
            )
            : Container(),

          ],
        ),
      ),
    );
  }


  String qualityMapping(YoutubeQuality quality) {
    switch (quality) {
      case YoutubeQuality.LOWEST:
        return '144p';
      case YoutubeQuality.LOW:
        return '240p';
      case YoutubeQuality.MEDIUM:
        return '360p';
      case YoutubeQuality.HIGH:
        return '480p';
      case YoutubeQuality.HD:
        return '720p';
      case YoutubeQuality.FHD:
        return '1080p';
      default:
        return "Invalid Quality";
    }
  }


  String getIdFromUrl(String url, [bool trimWhitespaces = true]) {
    if (url == null || url.length == 0) {
      return null;
    }

    if (trimWhitespaces) {
      url = url.trim();
    }

    for (var exp in _regexps) {
      Match match = exp.firstMatch(url);
      if (match != null && match.groupCount >= 1) return match.group(1);
    }

    return null;
  }


  List<RegExp> _regexps = [
    RegExp(r"^https:\/\/(?:www\.|m\.)?youtube\.com\/watch\?v=([_\-a-zA-Z0-9]{11}).*$"),
    RegExp(r"^https:\/\/(?:www\.|m\.)?youtube(?:-nocookie)?\.com\/embed\/([_\-a-zA-Z0-9]{11}).*$"),
    RegExp(r"^https:\/\/youtu\.be\/([_\-a-zA-Z0-9]{11}).*$")
  ];


  Future<dynamic> _pushFullScreenWidget(BuildContext context, [bool triggeredByUser = true]) async {
    final TransitionRoute<Null> route = PageRouteBuilder<Null>(
      settings: RouteSettings(isInitialRoute: false),
      pageBuilder: _fullScreenRoutePageBuilder,
    );

    SystemChrome.setEnabledSystemUIOverlays([]);
    if (triggeredByUser) {
      SystemChrome.setPreferredOrientations([
        DeviceOrientation.landscapeLeft,
        DeviceOrientation.landscapeRight,
      ]);
      _triggeredByUser = true;
    }

    _isFullScreen = true;
    await Navigator.of(context).push(route);
    _isFullScreen = false;

    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
    if (triggeredByUser) {
      SystemChrome.setPreferredOrientations(
        const [
          DeviceOrientation.portraitUp,
          DeviceOrientation.portraitDown,
          DeviceOrientation.landscapeLeft,
          DeviceOrientation.landscapeRight,
        ],
      );
      _triggeredByUser = false;
    }
  }

  Widget _fullScreenRoutePageBuilder(BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation,) {
    return AnimatedBuilder(
      animation: animation,
      builder: (BuildContext context, Widget child) {
        return Scaffold(
          body: _buildVideo(MediaQuery.of(context).size.height, MediaQuery.of(context).size.width, true),
        );
      },
    );
  }

}