import 'package:flutter/material.dart';
import 'package:youtube_native_player/android/video_player_controller.dart';

class VideoPlayer extends StatefulWidget {
  VideoPlayer(this.controller);
  final VideoPlayerController controller;
  @override
  _VideoPlayerState createState() => _VideoPlayerState();
}

class _VideoPlayerState extends State<VideoPlayer> {
  _VideoPlayerState() {
    _listener = () {
      // ignore: invalid_use_of_visible_for_testing_member
      final int newTextureId = widget.controller.textureId;
      //print("_listener > newTextureId : " + newTextureId.toString());
      if (newTextureId != _textureId) {
        setState(() {
          _textureId = newTextureId;
        });
      }
    };
  }

  VoidCallback _listener;
  int _textureId;

  @override
  void initState() {
    super.initState();
    // ignore: invalid_use_of_visible_for_testing_member
    _textureId = widget.controller.textureId;
    widget.controller.addListener(_listener);
  }

  @override
  void didUpdateWidget(VideoPlayer oldWidget) {
    super.didUpdateWidget(oldWidget);
    oldWidget.controller.removeListener(_listener);
    // ignore: invalid_use_of_visible_for_testing_member
    _textureId = widget.controller.textureId;
    widget.controller.addListener(_listener);
  }

  @override
  void deactivate() {
    super.deactivate();
    widget.controller.removeListener(_listener);
  }

  @override
  Widget build(BuildContext context) {
    return _textureId == null ? Container() : Texture(textureId: _textureId);
  }
}