import 'package:youtube_native_player/android/video_player_controller.dart';

typedef YPCallBack(VideoPlayerController controller);
typedef ErrorCallback(String error);
typedef NumberCallback(int duration);