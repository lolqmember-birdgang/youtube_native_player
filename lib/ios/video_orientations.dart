import 'package:flutter/material.dart';
import 'package:flutter/services.dart';



class VideoOrientations {
  const VideoOrientations({
    this.partScreenOrientations = const [
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ],
    this.fullScreenOrientations = const [
      DeviceOrientation.landscapeRight,
      DeviceOrientation.landscapeLeft,
    ],
  });


  final List<DeviceOrientation> partScreenOrientations;
  final List<DeviceOrientation> fullScreenOrientations;


  partScreen() {
    SystemChrome.setPreferredOrientations(partScreenOrientations);
  }

  fullScreen() {
    SystemChrome.setPreferredOrientations(fullScreenOrientations);
  }
}