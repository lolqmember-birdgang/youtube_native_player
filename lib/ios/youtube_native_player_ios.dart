import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:youtube_native_player/constants/youtube_native_player_constant.dart';
import 'package:youtube_native_player/ios/video_orientations.dart';


typedef void FlutterYoutubeViewCreatedCallback(YoutubeNativePlayerController controller);

class YoutubeNativePlayerIOS extends StatefulWidget {
  YoutubeNativePlayerIOS({
    @required this.source,
    this.aspectRatio = 16 / 9,
    this.width,
    this.onError,
    this.onVideoStarted,
    this.onVideoEnded,
    this.videoOrientations,
    Key key,
  }) : super(key: key);

  final String source;
  final double aspectRatio;
  final double width;
  final ErrorCallback onError;
  final NumberCallback onVideoStarted;
  final VoidCallback onVideoEnded;
  final VideoOrientations videoOrientations;

  @override
  State<StatefulWidget> createState() => _YoutubeNativePlayerIOSState();
}


class _YoutubeNativePlayerIOSState extends State<YoutubeNativePlayerIOS> implements YoutubeNativePlayerListener {
  YoutubeNativePlayerController _controller;
  String _state = "";
  String _currentDate = "";
  String _durationDate = "";
  double _sliderValue = 0.0;
  double _durationSecond = 0.0;

  double _mediaWidth = 0.0;
  double _width = 0.0;
  double _height = 0.0;

  bool _videoStaredCalled = false;
  bool _videoEndedCalled = false;

  double _opacity = 1.0;
  bool _isOpacity = false;
  Timer _timerOpacity;


  /**
   * override
   */
  @override
  void initState() {
    super.initState();
    initView();
  }

  @override
  void dispose() {
    super.dispose();
  }


  void initView() {
    Size mediaSize = MediaQuery
        .of(context)
        .size;

    if (mediaSize.width != _mediaWidth) {
      if (null != widget.width) {
        _mediaWidth = mediaSize.width;
        _width = widget.width;
        _height = _width / widget.aspectRatio;
      } else {
        _mediaWidth = mediaSize.width;
        _width = mediaSize.width;
        _height = _width / widget.aspectRatio;
      }
    }
  }


  /**
   * widget
   */
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: _width,
      height: _height,
      child: _stackWidget(),
    );
  }


  changeOpacity() {
    _isOpacity = !_isOpacity;
    if (_isOpacity) {
      duringOpacityTimer();
    }
  }


  showOpacity() {
    _isOpacity = true;
  }


  twinkleOpacity() {
    _isOpacity = true;
    duringOpacityTimer();
  }


  duringOpacityTimer() async {
    if (null != _timerOpacity) {
      _timerOpacity.cancel();
    }
    _timerOpacity = Timer(Duration(seconds: 4), endOpacityTimer);
    return _timerOpacity;
  }


  endOpacityTimer() {
    _isOpacity = false;
    setState(() {});
  }


  Widget _stackWidget() {
    return Stack(
      children: <Widget>[
        _youtubeViewWidget(),
        Container(
          child: InkWell(
            onTap: () {
              changeOpacity();
              setState(() {});
            },
          ),
        ),
        AnimatedOpacity(
          opacity: _isOpacity ? 1.0 : 0.0,
          duration: Duration(seconds: 1),
          child: Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              width: _width,
              child: Row(
                children: <Widget>[
                  Expanded(
                    flex: 3,
                    child: Text(
                      _currentDate.toString(),
                      style: TextStyle(color: Colors.white),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  Expanded(
                    flex: 13,
                    child: Container(
                      height: 50.0,
                      child: Slider(
                        value: _sliderValue,
                        max: _durationSecond,
                        onChangeStart: (value) {
                          _controller.pause();
                          showOpacity();
                          setState(() {});
                        },
                        onChanged: (value) {
                          setState(() {
                            _sliderValue = value;
                            _controller.seekTo(value);
                            showOpacity();
                          });
                        },
                        onChangeEnd: (value) {
                          setState(() {
                            _sliderValue = value;
                            _controller.seekTo(value);
                            _controller.play();
                            twinkleOpacity();
                            setState(() {});
                          });
                        },
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 3,
                    child: AnimatedOpacity(
                      opacity: _isOpacity ? 1.0 : 0.0,
                      duration: Duration(milliseconds: 500),
                      child: Text(
                        _durationDate.toString(),
                        style: TextStyle(color: Colors.white),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 3,
                    child: AnimatedOpacity(
                      opacity: _isOpacity ? 1.0 : 0.0,
                      duration: Duration(milliseconds: 500),
                      child: IconButton(
                        color: Colors.white,
                        alignment: Alignment.center,
                        icon: Icon(Icons.fullscreen),
                        onPressed: () {
                          widget.videoOrientations.fullScreen();
                          _controller.fullScreen();
                        },
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        AnimatedOpacity(
          opacity: _isOpacity ? 1.0 : 0.0,
          duration: Duration(seconds: 1),
          child: Center(
            child: ("play" == _state)
                ?
            IconButton(
              iconSize: 50.0,
              icon: Icon(Icons.pause),
              alignment: Alignment.center,
              color: Colors.white,
              onPressed: () {
                _controller.pause();
                twinkleOpacity();
                setState(() {});
              },
            ) :
            IconButton(
              iconSize: 50.0,
              icon: Icon(Icons.play_arrow),
              alignment: Alignment.center,
              color: Colors.white,
              onPressed: () {
                _controller.play();
                twinkleOpacity();
                setState(() {});
              },
            ),
          ),
        ),
      ],
    );
  }

  Widget _youtubeViewWidget() {
    return UiKitView(
      viewType: "com.lolq.youtubenativeplayer",
      onPlatformViewCreated: _onPlatformViewCreated,
      creationParams: <String, dynamic>{
        "youtubeId": widget.source,
        "showUI": true,
        "startSeconds": 0.0
      },
      creationParamsCodec: StandardMessageCodec(),
    );
  }


  /**
   * function
   */
  void _onPlatformViewCreated(int id) {
    print("youtube_native_player_ios > _onPlatformViewCreated: " + id.toString());
    _controller = new YoutubeNativePlayerController.of(id, this);
    _controller.initialization();
  }


  /**
   * override
   */
  @override
  void onError(String error) {
    print("onError error = $error");
  }

  @override
  void onReady() {
    print("onReady");
  }

  @override
  void onStateChange(String state) {
    print("onStateChange state = $state");
    setState(() {
      _state = state;
    });
  }

  @override
  void onStarted(int duration) {
    print("onStarted duration = $duration");
    if (!_videoStaredCalled) {
      _videoStaredCalled = true;
      widget.onVideoStarted(duration);
    }
    setState(() {});
  }

  @override
  void onVideoDuration(double duration) {
    print("onVideoDuration duration = $duration");
    setState(() {
      _durationSecond = duration;
      _durationDate = (duration ~/ 60.0).toString() + ":" + (duration % 60.0).toInt().toString();
    });
  }

  @override
  void onCurrentSecond(double second) {
    double rvsSecond = (0.0 < second) ? (_durationSecond < second) ? _durationSecond : second : 0.0;
    setState(() {
      _sliderValue = (0.0 < rvsSecond) ? second : 0.0;
      _currentDate = (rvsSecond ~/ 60.0).toString() + ":" + (rvsSecond % 60.0).toInt().toString();
    });
  }

  @override
  void onFullScreen(bool full) {
    print("onFullScreen full = $full");
    setState(() {
      widget.videoOrientations.partScreen();
      _controller.partScreen();
    });
  }
}


abstract class YoutubeNativePlayerListener {
  void onReady();
  void onStateChange(String state);
  void onError(String error);
  void onStarted(int duration);
  void onVideoDuration(double duration);
  void onCurrentSecond(double second);
  void onFullScreen(bool full);
}


enum YoutubeNativePlayerScaleMode { none, fitWidth, fitHeight }

class YoutubeNativePlayerController {
  final MethodChannel _channel;
  final YoutubeNativePlayerListener _listener;

  YoutubeNativePlayerController.of(int id, YoutubeNativePlayerListener listener)
      : _channel = new MethodChannel("com.lolq.youtubenativeplayer/$id"),
        _listener = listener {
    if (_listener != null) {
      _channel.setMethodCallHandler(handleEvent);
    }
  }

  Future<void> initialization() async {
    await _channel.invokeMethod('initialization');
  }

  Future<void> loadOrCueVideo(String videoId, double startSeconds) async {
    assert(videoId != null);
    var params = <String, dynamic>{
      "videoId": videoId,
      "startSeconds": startSeconds
    };
    await _channel.invokeMethod('loadOrCueVideo', params);
  }

  Future<void> play() async {
    await _channel.invokeMethod('play', null);
  }

  Future<void> pause() async {
    await _channel.invokeMethod('pause', null);
  }

  Future<void> seekTo(double time) async {
    await _channel.invokeMethod('seekTo', time);
  }

  Future<void> setVolume(int volumePercent) async {
    await _channel.invokeMethod('setVolume', volumePercent);
  }

  Future<void> setMute() async {
    await _channel.invokeMethod('mute', null);
  }

  Future<void> setUnMute() async {
    await _channel.invokeMethod('unMute', null);
  }

  Future<void> partScreen() async {
    await _channel.invokeMethod('partScreen', null);
  }

  Future<void> fullScreen() async {
    await _channel.invokeMethod('fullScreen', null);
  }

  Future<void> changeScale(double width, double height) async {
    var params = <String, dynamic>{
      "width": width,
      "height": height
    };
    await _channel.invokeMethod('changeScale', params);
  }

  Future<void> changeScaleMode(YoutubeNativePlayerScaleMode mode) async {
    await _channel.invokeMethod('scaleMode', mode.index);
  }

  Future<dynamic> handleEvent(MethodCall call) async {
    switch (call.method) {
      case 'onReady':
        _listener.onReady();
        break;
      case 'onStateChange':
        _listener.onStateChange(call.arguments as String);
        break;
      case 'onError':
        _listener.onError(call.arguments as String);
        break;
      case 'onStarted':
        _listener.onStarted(call.arguments as int);
        break;
      case 'onVideoDuration':
        _listener.onVideoDuration(call.arguments as double);
        break;
      case 'onCurrentSecond':
        _listener.onCurrentSecond(call.arguments as double);
        break;
      case 'onFullScreen':
        _listener.onFullScreen(call.arguments as bool);
        break;
    }
    return null;
  }
}